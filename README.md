# FaultSys Crash Reporting System

This is a barebones system that can be used for collecting dumps and logs from programs written in C++ on Windows platform. It was part of my [thesis (in Finnish)][1] regarding design and implementation of such system. The system is rather simple and is not documented apart from the thesis. Its intended purpose is to provide a base for more complete implementations and to serve the needs of my other personal projects. The system consists of three modules.

__FaultLib__ is a DLL which is to be used by other software. Main features are a logger implementation, a default exception handler and automatic minidumps.

__FaultReport__ is a program written in C#/.NET that creates, manages and sends crash reports. It can be invoked manually or by FaultLib exception handler. It features a GUI and functionality to cover most usual use cases.

__FaultApi__ is a REST API implementation for Azure Functions. Its sole purpose is to receive crash reports and store dumps and logs into blob containers.

## Dependencies

FaultSys relies on functionality from Windown API, [Debug Help Library][2] and some utilities from my [DiagUtils][3] library.

[1]: http://urn.fi/URN:NBN:fi:amk-201905109081
[2]: https://docs.microsoft.com/en-us/windows/desktop/debug/debug-help-library
[3]: https://gitlab.com/nagsucc/diag-utils