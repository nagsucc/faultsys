// FaultReport.cpp : Defines the exported functions for the DLL application.

#include "FaultLib.h"
#include "Handler.h"

#include <DiagUtils.h>
#pragma comment(lib, "DiagUtils.lib")

FAULT_CONFIG g_faultLibConfiguration = { 0 };
std::string g_errorMessage = "";
SysInfo* g_pSysInfo = nullptr;

void faultInit(FAULT_CONFIG& config)
{
	faultLogDebugStream << "FaultLib version " << FAULT_LIB_VERSION_STRING << "\n";

	static SysInfo sysInfo;
	g_pSysInfo = &sysInfo;

	g_faultLibConfiguration = std::move(config);
	
	FaultHandler handler = DefaultHandler;
	if (g_faultLibConfiguration.pfnHandler != nullptr)
	{
		handler = g_faultLibConfiguration.pfnHandler;
	}

	SetUnhandledExceptionFilter(handler);

	faultLogDebugStream
		<< std::left
		<< "\n=== FaultLib Init ===\n"
		<< std::setw(32) << "AppId: "				<< config.szAppId << "\n"
		<< std::setw(32) << "Application Name: "	<< config.szApplicationName << "\n"
		<< std::setw(32) << "Application Version: " << config.szApplicationVersion << "\n"
		<< std::setw(32) << "Working Directory: "	<< config.szWorkingDirectory << "\n"
		<< std::setw(32) << "Mode: "				<< (config.modeFlag == FaultModeFlag::kModeSilent ? "Silent" : "GUI") << "\n"
		<< std::setw(32) << "Offline Cache: "		<< (config.settingsFlags & FaultSettingsFlag::kUseOfflineCache ? "ENABLED" : "DISABLED") << "\n"
		<< std::setw(32) << "Clear Files: "			<< (config.settingsFlags & FaultSettingsFlag::kClearFiles ? "ENABLED" : "DISABLED") << "\n"
		<< std::setw(32) << "Dumps: "				<< (config.reportFlags & FaultReportFlag::kIncludeDump ? "ENABLED" : "DISABLED") << "\n"
		<< std::setw(32) << "Logs: "				<< (config.reportFlags & FaultReportFlag::kIncludeLog ? "ENABLED" : "DISABLED") << "\n"
		<< std::setw(32) << "Stack Trace: "			<< (config.reportFlags & FaultReportFlag::kIncludeStackTrace ? "ENABLED" : "DISABLED") << "\n"
		<< std::setw(32) << "System Info: "			<< (config.reportFlags & FaultReportFlag::kIncludeSystemInfo ? "ENABLED" : "DISABLED") << "\n"
		<< "=== End of FaultLib Init ===\n";
}

void faultUninit()
{
	SetUnhandledExceptionFilter(NULL);
}

bool faultOpenDialog(
	const char * szMessage, 
	const char * szDumpPath, 
	const char * szLogPath)
{
	std::string commandLine = g_faultLibConfiguration.szWorkingDirectory;
	commandLine.append("\\").append(FAULT_REPORTER_BIN);

	if (Utils::FileExists(commandLine) == false)
	{
		// Try current directory
		commandLine = Utils::GetCurrentPath();
		commandLine.append("\\").append(FAULT_REPORTER_BIN);
	}

	if (Utils::FileExists(commandLine) == false)
	{
		// Try module directory
		std::string module = Utils::GetModulePath();
		commandLine = Utils::GetDirectory(module);
		commandLine.append("\\").append(FAULT_REPORTER_BIN);
	}

	if (Utils::FileExists(commandLine) == false)
	{
		return false; // Give up
	}

	// Set params
	const FAULT_CONFIG& config = _faultConfig_get();

	if (config.modeFlag == FaultModeFlag::kModeSilent)	
		commandLine.append(" --silent");
	else					
		commandLine.append(" --gui");

	if (!(config.reportFlags & FaultReportFlag::kDisableReports))
		commandLine.append(" --send-report");

	if (config.settingsFlags & FaultSettingsFlag::kUseOfflineCache)		commandLine.append(" --offline-cache");
	if (!(config.settingsFlags & FaultSettingsFlag::kClearFiles))		commandLine.append(" --preserve-files");

	commandLine
		.append(" --appid \"").append(g_faultLibConfiguration.szAppId).append("\"")
		.append(" --appver \"").append(g_faultLibConfiguration.szApplicationVersion).append("\"");

	if (strlen(szMessage) > 0)
	{
		commandLine.append(" --message \"").append(szMessage).append("\"");
	}

	if (strlen(szDumpPath) > 0)
	{
		commandLine.append(" --dump \"").append(szDumpPath).append("\"");
	}

	if (strlen(szLogPath) > 0)
	{
		commandLine.append(" --log \"").append(szLogPath).append("\"");
	}

	LOG_TRACE(commandLine);

	STARTUPINFO si = {};
	PROCESS_INFORMATION pi = {};

	return CreateProcess(
		NULL, (char*) commandLine.c_str(),
		NULL, NULL,
		FALSE, 0, NULL, NULL,
		&si, &pi);
}

void faultSetMessage(const char * szMessage)
{
	g_errorMessage = szMessage;
}

bool faultDump(PEXCEPTION_POINTERS pException, const char * szPath, MINIDUMP_TYPE type)
{
	return Dump::Write(pException, szPath, type);
}

std::ostream & faultGetLogStream(int level, const char * szFile, int line)
{
	return Log::GetOutputStream(static_cast<LogLevel>(level), szFile, line);
}

void faultSetLogLevel(int level)
{
	std::set<LogLevel> levels;

	if (level > FAULT_LOG_LEVEL_DISABLE)
	{
		if (level >= FAULT_LOG_LEVEL_ERROR)	levels.emplace(LogLevel::Error);
		if (level >= FAULT_LOG_LEVEL_WARNING) levels.emplace(LogLevel::Warning);
		if (level >= FAULT_LOG_LEVEL_INFO) levels.emplace(LogLevel::Info);
		if (level >= FAULT_LOG_LEVEL_DEBUG) levels.emplace(LogLevel::Debug);
		if (level >= FAULT_LOG_LEVEL_TRACE) levels.emplace(LogLevel::Trace);
	}

	Log::SetLevels(levels);
}

void faultSetLogFilePath(const char* szPath)
{
	Log::SetFilePath(szPath);
}

void faultSetLogOutputStream(std::ostream& stream)
{
	Log::SetStream(stream);
}

const char * _faultMessage_get()
{
	return g_errorMessage.c_str();
}

const FAULT_CONFIG & _faultConfig_get()
{
	return g_faultLibConfiguration;
}

void* _faultSysInfo_get()
{
	return reinterpret_cast<void*>(g_pSysInfo);
}