#pragma once
#define FAULT_LIB_VERSION_STRING "1.1.2"
#define FAULT_LIB_VERSION_MAJOR 1
#define FAULT_LIB_VERSION_MINOR 1
#define FAULT_LIB_VERSION_PATCH 2

#define FAULT_LOG_LEVEL_DISABLE -1
#define FAULT_LOG_LEVEL_ERROR 0
#define FAULT_LOG_LEVEL_WARNING 1
#define FAULT_LOG_LEVEL_INFO 2
#define FAULT_LOG_LEVEL_DEBUG 3
#define FAULT_LOG_LEVEL_TRACE 4

#ifdef FaultLib_EXPORTS
#define FAULTLIB __declspec(dllexport)
#else
#define FAULTLIB __declspec(dllimport)
#endif

#include <iostream>

#ifndef WIN32
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#endif

#include <DbgHelp.h>
#include <minidumpapiset.h>
#pragma comment (lib, "dbghelp.lib")

#define FAULT_REPORTER_BIN "FaultReport.exe"

struct FAULT_CALLBACK_DATA
{
	PEXCEPTION_POINTERS pExceptionInfo;
	const char* szDumpPath;
	const char* szLogPath;
};

using FaultHandler = LONG(*)(PEXCEPTION_POINTERS);
using FaultCallback = void(*)(FAULT_CALLBACK_DATA*);

enum class FaultModeFlag : uint32_t
{
	kModeSilent = 0,
	kModeGui = 1
};

enum class FaultSettingsFlag : uint32_t
{
	kUseOfflineCache = 1,
	kClearFiles = 1 << 1,
	kDefault = kUseOfflineCache | kClearFiles
};

enum class FaultReportFlag : uint32_t
{
	kDisableReports = 1,
	kIncludeDump = 1 << 1,
	kIncludeLog = 1 << 2,
	kIncludeStackTrace = 1 << 3,
	kIncludeSystemInfo = 1 << 4,
	kDefault = 
		kIncludeDump | kIncludeLog | 
		kIncludeStackTrace | kIncludeSystemInfo
};

constexpr enum FaultSettingsFlag operator |(const enum FaultSettingsFlag selfValue, const enum FaultSettingsFlag inValue)
{
	return (enum FaultSettingsFlag)(uint32_t(selfValue) | uint32_t(inValue));
}

constexpr bool operator &(const enum FaultSettingsFlag selfValue, const enum FaultSettingsFlag inValue)
{
	return (uint32_t(selfValue) & uint32_t(inValue)) == uint32_t(inValue);
}

constexpr enum FaultReportFlag operator |(const enum FaultReportFlag selfValue, const enum FaultReportFlag inValue)
{
	return (enum FaultReportFlag)(uint32_t(selfValue) | uint32_t(inValue));
}

constexpr bool operator &(const enum FaultReportFlag selfValue, const enum FaultReportFlag inValue)
{
	return (uint32_t(selfValue) & uint32_t(inValue)) == uint32_t(inValue);
}

struct FAULT_CONFIG
{
	const char* szAppId;
	const char* szApplicationName;
	const char* szApplicationVersion;
	const char* szWorkingDirectory;

	FaultModeFlag modeFlag;
	FaultSettingsFlag settingsFlags;
	FaultReportFlag reportFlags;

	FaultHandler pfnHandler;
	FaultCallback pfnCallback;
	MINIDUMP_TYPE minidumpType;

	FAULT_CONFIG(
		const char* appId = "",
		const char* appName = "",
		const char* appVersion = "",
		const char* directory = "",
		FaultModeFlag modeFlag = FaultModeFlag::kModeGui,
		FaultSettingsFlag settingsFlags = FaultSettingsFlag::kDefault,
		FaultReportFlag reportFlags = FaultReportFlag::kDefault,
		FaultHandler handler = nullptr,
		FaultCallback callback = nullptr,
		MINIDUMP_TYPE minidumpType = MiniDumpNormal
	)
		: szAppId(appId)
		, szApplicationName(appName)
		, szApplicationVersion(appVersion)
		, szWorkingDirectory(directory)
		, pfnHandler(handler)
		, pfnCallback(callback)
		, minidumpType(minidumpType)
		, modeFlag(modeFlag)
		, settingsFlags(settingsFlags)
		, reportFlags(reportFlags)
	{}
};

extern "C"
{
	/* Initialization */
	FAULTLIB void faultInit(FAULT_CONFIG& config);
	FAULTLIB void faultUninit();

	/* Report dialog */
	FAULTLIB bool faultOpenDialog(
		const char* szMessage = "", 
		const char* szDumpPath = "", 
		const char* szLogPath = "");

	FAULTLIB void faultSetMessage(const char* szMessage);

	/* Dump */
	FAULTLIB bool faultDump(
		PEXCEPTION_POINTERS pException,
		const char* szPath,
		MINIDUMP_TYPE type = MiniDumpNormal);

	/* Logger */
	FAULTLIB std::ostream& faultGetLogStream(int level, const char* szFile, int line);
	FAULTLIB void faultSetLogLevel(int level);
	FAULTLIB void faultSetLogFilePath(const char* szPath);
	FAULTLIB void faultSetLogOutputStream(std::ostream& stream);
};

/* Log Stream Macros */
#define faultLogDebugStream		faultGetLogStream(FAULT_LOG_LEVEL_DEBUG, __FILE__, __LINE__)
#define faultLogInfoStream		faultGetLogStream(FAULT_LOG_LEVEL_INFO, __FILE__, __LINE__)
#define faultLogWarningStream	faultGetLogStream(FAULT_LOG_LEVEL_WARNING, __FILE__, __LINE__)
#define faultLogErrorStream		faultGetLogStream(FAULT_LOG_LEVEL_ERROR, __FILE__, __LINE__)
#define faultLogTraceStream		faultGetLogStream(FAULT_LOG_LEVEL_TRACE, __FILE__, __LINE__)

/* Log Line Macros */
#define faultLogDebug(x)	faultLogDebugStream << x << std::endl
#define faultLogInfo(x)		faultLogInfoStream << x << std::endl
#define faultLogWarning(x)	faultLogWarningStream << x << std::endl
#define faultLogError(x)	faultLogErrorStream << x << std::endl
#define faultLogTrace(x)	faultLogTraceStream << x << std::endl

#ifdef FaultLib_EXPORTS
/* Internal */
const FAULT_CONFIG& _faultConfig_get();
const char* _faultMessage_get();
void* _faultSysInfo_get();
#endif
