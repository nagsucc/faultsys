#include <DiagUtils.h>
#include "FaultLib.h"
#include "Handler.h"

LONG WINAPI DefaultHandler(PEXCEPTION_POINTERS pExceptionInfo)
{
	const FAULT_CONFIG& config = _faultConfig_get();

	LOG_ERROR("Fatal exception: " << std::hex << pExceptionInfo->ExceptionRecord->ExceptionCode);

	std::string outputPath = config.szWorkingDirectory;
	std::string moduleName = config.szApplicationName;

	// Generate file name
	tm t = Utils::GetUtcTime();
	std::string id = Utils::BytesToHexString(Utils::GetRandomBytes(8));

	std::string fileName;
	{
		std::stringstream ss;
		ss << std::setfill('0')
			<< moduleName.c_str() 
			<< "_"
			<< (t.tm_year + 1900) 
			<< std::setw(2)<< (t.tm_mon + 1)
			<< std::setw(2) << t.tm_mday
			<< std::setw(2) << t.tm_hour 
			<< std::setw(2) << t.tm_min 
			<< "_"
			<< id;
		fileName = ss.str();
	}

	Utils::CreateSubDirectory(outputPath);

	// Make a minidump
	std::string dumpPath = "";
	if (config.reportFlags & FaultReportFlag::kIncludeDump)
	{
		dumpPath = outputPath + fileName + ".dmp";

		if (Dump::Write(pExceptionInfo, dumpPath, config.minidumpType) == false)
		{
			LOG_ERROR("Failed to write crash dump!");
			dumpPath = "";
		}
	}

	// Gather data
	std::stringstream logData;
	bool hasData = false;

	if (config.reportFlags & FaultReportFlag::kIncludeLog)
	{
		hasData = true;

		std::istream& in = Log::GetInputStream();
		char buffer[4096];

		while (in.read(buffer, sizeof(buffer)))
		{
			logData.write(buffer, sizeof(buffer));
		}
		logData.write(buffer, in.gcount());
	}

	if (config.reportFlags & FaultReportFlag::kIncludeSystemInfo)
	{
		hasData = true;
		SysInfo* pSysInfo = reinterpret_cast<SysInfo*>(_faultSysInfo_get());
		pSysInfo->GetSystemReport(logData);
	}

	if (config.reportFlags & FaultReportFlag::kIncludeStackTrace)
	{
		hasData = true;
		StackTrace::Get(pExceptionInfo->ContextRecord, logData);
	}

	// Write the log
	std::string logPath = "";
	if (hasData)
	{
		logPath = outputPath + fileName + ".log";

		std::fstream log(logPath, std::ios::trunc | std::ios::out);
		if (log.is_open())
		{
			log << logData.str();
			log.close();
		}
		else
		{
			logPath = "";
		}
	}

	// Invoke callback
	if (config.pfnCallback)
	{
		FAULT_CALLBACK_DATA data;
		data.pExceptionInfo = pExceptionInfo;
		data.szDumpPath = dumpPath.c_str();
		data.szLogPath = logPath.c_str();

		config.pfnCallback(&data);
	}

	// Execute reporter
	faultOpenDialog(_faultMessage_get(), dumpPath.c_str(), logPath.c_str());

	std::terminate();
	return EXCEPTION_CONTINUE_SEARCH;
}
