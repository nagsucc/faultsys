﻿using System;
using System.IO;

namespace FaultReport
{
	public class Report
	{
		public string AppId { get; set; }
		public string AppVersion { get; set; }
		public string Message { get; set; }
		public string DumpPath { get; set; }
		public string LogPath { get; set; }
		public DateTime Created { get; set; }

		public void DeleteFiles()
		{
			if (string.IsNullOrEmpty(DumpPath) == false)
			{
				try
				{
					File.Delete(DumpPath);
				}
				catch (Exception ex)
				{
					Console.WriteLine($"Failed to delete {DumpPath}: {ex.Message}");
				}
			}

			if (string.IsNullOrEmpty(LogPath) == false)
			{
				try
				{
					File.Delete(LogPath);
				}
				catch (Exception ex)
				{
					Console.WriteLine($"Failed to delete {LogPath}: {ex.Message}");
				}
			}
		}
	}
}
