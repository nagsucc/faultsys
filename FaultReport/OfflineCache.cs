﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;

namespace FaultReport
{
	static class OfflineCache
	{
		private static readonly string fileName = "FaultReport.pending";

		public static List<Report> GetPending()
		{
			var list = new List<Report>();

			if (File.Exists(fileName) == false)
			{
				return list;
			}

			try
			{
				var xml = XElement.Load(fileName);
				var reports = xml.Descendants("report");
				foreach (var item in reports)
				{
					var report = new Report
					{
						AppId = item.Element("appid").Value,
						Created = DateTime.Parse(item.Element("created").Value),
						AppVersion = item.Element("appver").Value,
						Message = item.Element("message").Value,
						DumpPath = item.Element("dump").Value,
						LogPath = item.Element("log").Value
					};

					list.Add(report);
				}
			}
			catch (Exception)
			{
			}

			return list;
		}

		public static void StorePending(List<Report> reports)
		{
			if (File.Exists(fileName))
			{
				File.Delete(fileName);
			}

			if (reports.Count > 0)
			{
				using (var writer = XmlWriter.Create(fileName))
				{
					var xml = new XElement("reports");
					foreach (var report in reports)
					{
						xml.Add(new XElement("report",
							new XElement("appid", report.AppId),
							new XElement("created", report.Created.ToString("o")),
							new XElement("appver", report.AppVersion ?? ""),
							new XElement("message", report.Message ?? ""),
							new XElement("dump", report.DumpPath ?? ""),
							new XElement("log", report.LogPath ?? "")));
					}
					xml.WriteTo(writer);
					writer.Close();
				}
			}
		}
	}
}
