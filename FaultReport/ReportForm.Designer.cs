﻿namespace FaultReport
{
	partial class ReportForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportForm));
			this.labelHeader = new System.Windows.Forms.Label();
			this.panelContent = new System.Windows.Forms.Panel();
			this.textBoxDetails = new System.Windows.Forms.TextBox();
			this.buttonSendReport = new System.Windows.Forms.Button();
			this.panelBottom = new System.Windows.Forms.Panel();
			this.buttonClose = new System.Windows.Forms.Button();
			this.buttonCancel = new System.Windows.Forms.Button();
			this.panelTop = new System.Windows.Forms.Panel();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.panelContent.SuspendLayout();
			this.panelBottom.SuspendLayout();
			this.panelTop.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// labelHeader
			// 
			this.labelHeader.Dock = System.Windows.Forms.DockStyle.Fill;
			this.labelHeader.Font = new System.Drawing.Font("Calibri Light", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelHeader.ForeColor = System.Drawing.SystemColors.ControlLightLight;
			this.labelHeader.Location = new System.Drawing.Point(6, 6);
			this.labelHeader.Name = "labelHeader";
			this.labelHeader.Size = new System.Drawing.Size(465, 72);
			this.labelHeader.TabIndex = 0;
			this.labelHeader.Text = "An error occured";
			this.labelHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// panelContent
			// 
			this.panelContent.BackColor = System.Drawing.SystemColors.Control;
			this.panelContent.Controls.Add(this.textBoxDetails);
			this.panelContent.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelContent.Location = new System.Drawing.Point(0, 84);
			this.panelContent.Name = "panelContent";
			this.panelContent.Padding = new System.Windows.Forms.Padding(6);
			this.panelContent.Size = new System.Drawing.Size(525, 167);
			this.panelContent.TabIndex = 1;
			// 
			// textBoxDetails
			// 
			this.textBoxDetails.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.textBoxDetails.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textBoxDetails.Location = new System.Drawing.Point(6, 6);
			this.textBoxDetails.Multiline = true;
			this.textBoxDetails.Name = "textBoxDetails";
			this.textBoxDetails.ReadOnly = true;
			this.textBoxDetails.Size = new System.Drawing.Size(513, 155);
			this.textBoxDetails.TabIndex = 9001;
			this.textBoxDetails.TabStop = false;
			this.textBoxDetails.Text = "An error occured and the program was shutdown.";
			// 
			// buttonSendReport
			// 
			this.buttonSendReport.Dock = System.Windows.Forms.DockStyle.Right;
			this.buttonSendReport.Location = new System.Drawing.Point(359, 6);
			this.buttonSendReport.Name = "buttonSendReport";
			this.buttonSendReport.Size = new System.Drawing.Size(160, 44);
			this.buttonSendReport.TabIndex = 2;
			this.buttonSendReport.Text = "Send report";
			this.buttonSendReport.UseVisualStyleBackColor = true;
			this.buttonSendReport.Click += new System.EventHandler(this.buttonSendReport_Click);
			// 
			// panelBottom
			// 
			this.panelBottom.BackColor = System.Drawing.SystemColors.ControlLight;
			this.panelBottom.Controls.Add(this.buttonClose);
			this.panelBottom.Controls.Add(this.buttonCancel);
			this.panelBottom.Controls.Add(this.buttonSendReport);
			this.panelBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panelBottom.Location = new System.Drawing.Point(0, 251);
			this.panelBottom.Name = "panelBottom";
			this.panelBottom.Padding = new System.Windows.Forms.Padding(6);
			this.panelBottom.Size = new System.Drawing.Size(525, 56);
			this.panelBottom.TabIndex = 3;
			// 
			// buttonClose
			// 
			this.buttonClose.Dock = System.Windows.Forms.DockStyle.Right;
			this.buttonClose.Location = new System.Drawing.Point(39, 6);
			this.buttonClose.Name = "buttonClose";
			this.buttonClose.Size = new System.Drawing.Size(160, 44);
			this.buttonClose.TabIndex = 4;
			this.buttonClose.Text = "Close";
			this.buttonClose.UseVisualStyleBackColor = true;
			this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
			// 
			// buttonCancel
			// 
			this.buttonCancel.Dock = System.Windows.Forms.DockStyle.Right;
			this.buttonCancel.Location = new System.Drawing.Point(199, 6);
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.Size = new System.Drawing.Size(160, 44);
			this.buttonCancel.TabIndex = 3;
			this.buttonCancel.Text = "Cancel";
			this.buttonCancel.UseVisualStyleBackColor = true;
			this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
			// 
			// panelTop
			// 
			this.panelTop.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.panelTop.Controls.Add(this.labelHeader);
			this.panelTop.Controls.Add(this.pictureBox1);
			this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelTop.Location = new System.Drawing.Point(0, 0);
			this.panelTop.Name = "panelTop";
			this.panelTop.Padding = new System.Windows.Forms.Padding(6);
			this.panelTop.Size = new System.Drawing.Size(525, 84);
			this.panelTop.TabIndex = 1;
			// 
			// pictureBox1
			// 
			this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Right;
			this.pictureBox1.Image = global::FaultReport.Properties.Resources.fault;
			this.pictureBox1.Location = new System.Drawing.Point(471, 6);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(48, 72);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox1.TabIndex = 1;
			this.pictureBox1.TabStop = false;
			// 
			// ReportForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.ClientSize = new System.Drawing.Size(525, 307);
			this.Controls.Add(this.panelContent);
			this.Controls.Add(this.panelTop);
			this.Controls.Add(this.panelBottom);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "ReportForm";
			this.Text = "Fault Report";
			this.Load += new System.EventHandler(this.ReportForm_Load);
			this.panelContent.ResumeLayout(false);
			this.panelContent.PerformLayout();
			this.panelBottom.ResumeLayout(false);
			this.panelTop.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label labelHeader;
		private System.Windows.Forms.Panel panelContent;
		private System.Windows.Forms.Button buttonSendReport;
		private System.Windows.Forms.Panel panelBottom;
		private System.Windows.Forms.Panel panelTop;
		private System.Windows.Forms.TextBox textBoxDetails;
		private System.Windows.Forms.Button buttonCancel;
		private System.Windows.Forms.Button buttonClose;
		private System.Windows.Forms.PictureBox pictureBox1;
	}
}

