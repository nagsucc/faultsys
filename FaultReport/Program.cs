﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace FaultReport
{
	using Properties;

	static class Program
	{
		public static bool GuiMode { get; private set; } = false;
		public static bool SilentMode { get; private set; } = false;
		public static bool SendReport { get; private set; } = false;
		public static bool ClearFiles { get; private set; } = true;
		public static bool UseOfflineCache { get; private set; } = false;

		private static Report report = null;

		[STAThread]
		static void Main(string[] args)
		{
			ParseArguments(args);
			report = ParseReport(args);

			if (GuiMode)
			{
				Application.EnableVisualStyles();
				Application.SetCompatibleTextRenderingDefault(false);
				Application.Run(new ReportForm(report));
			}
			else if (SilentMode)
			{
				if (SendReport)
				{
					var task = OnUpload();
					task.Wait();
				}
			}
			else
			{
				MessageBox.Show(Locale.MessageUsage, Locale.FormTitle);
			}
		}

		private static void ParseArguments(string[] args)
		{
			for (int i = 0; i < args.Length; i++)
			{
				if (args[i] == "--gui")
				{
					GuiMode = true;
				}
				else if (args[i] == "--silent")
				{
					SilentMode = true;
				}
				else if (args[i] == "--send-report")
				{
					SendReport = true;
				}
				else if (args[i] == "--preserve-files")
				{
					ClearFiles = false;
				}
				else if (args[i] == "--offline-cache")
				{
					UseOfflineCache = true;
				}
			}
		}

		private static Report ParseReport(string[] args)
		{
			string AppId = null;
			string AppVersion = null;
			string Message = null;
			string DumpPath = null;
			string LogPath = null;
			for (int i = 0; i < args.Length; i++)
			{
				if (args[i] == "--appid")
				{
					if (i + 1 < args.Length)
					{
						AppId = args[i + 1];
					}
				}
				else if (args[i] == "--appver")
				{
					if (i + 1 < args.Length)
					{
						AppVersion = args[i + 1];
					}
				}
				else if (args[i] == "--message")
				{
					if (i + 1 < args.Length)
					{
						Message = args[i + 1];
					}
				}
				else if (args[i] == "--dump")
				{
					if (i + 1 < args.Length)
					{
						if (File.Exists(args[i + 1]))
						{
							DumpPath = args[i + 1];
						}
					}
				}
				else if (args[i] == "--log")
				{
					if (i + 1 < args.Length)
					{
						if (File.Exists(args[i + 1]))
						{
							LogPath = args[i + 1];
						}
					}
				}
			}

			// Create a new report
			if (AppId != null)
			{
				return new Report
				{
					AppId = AppId,
					AppVersion = AppVersion,
					Message = Message,
					DumpPath = DumpPath,
					LogPath = LogPath,
					Created = DateTime.Now
				};
			}

			return null;
		}

		public static void OnCancel()
		{
			if (ClearFiles)
			{
				report?.DeleteFiles();
			}
		}

		public static async Task OnUpload()
		{
			var reports = new List<Report>();

			if(report != null)
			{
				reports.Add(report);
			}

			// Check offline cache
			if(UseOfflineCache)
			{
				reports.AddRange(OfflineCache.GetPending());
			}

			// Begin sending reports
			var sent = new List<Report>(reports.Count);

			foreach(var report in reports)
			{
				if (await Uploader.Send(report))
				{
					sent.Add(report);

					if (ClearFiles)
					{
						report.DeleteFiles();
					}
				}
			}

			// Remove sent reports and write offline cache
			if (UseOfflineCache)
			{
				reports.RemoveAll(x => sent.Contains(x));
				OfflineCache.StorePending(reports);
			}
		}
	}
}
