﻿using System;
using System.Threading.Tasks;
using System.Net.Http;
using System.IO;

namespace FaultReport
{
	class Uploader
	{
		private static readonly string apiUrl = "http://localhost:7071/api/v1/report";
		private static readonly string apiKey = "";

		public static async Task<bool> Send(Report report)
		{
			using (var client = new HttpClient())
			{
				var form = new MultipartFormDataContent
				{
					{ new StringContent(report.AppId), "appId" },
					{ new StringContent(report.Created.ToUniversalTime().ToString("o")), "created" },
					{ new StringContent(report.AppVersion ?? ""), "appVersion" },
					{ new StringContent(report.Message ?? ""), "message" }
				};

				AddFileToForm(form, report.DumpPath, "dump");
				AddFileToForm(form, report.LogPath, "log");

				form.Headers.Add("x-functions-key", apiKey);

				try
				{
					var response = await client.PostAsync(apiUrl, form);
					return response.IsSuccessStatusCode;
				}
				catch(Exception ex)
				{
					Console.WriteLine(ex.Message);
				}

				return false;
			}
		}

		private static void AddFileToForm(MultipartFormDataContent form, string path, string name)
		{
			if (string.IsNullOrEmpty(path) == false)
			{
				if (File.Exists(path))
				{
					var stream = new FileStream(path, FileMode.Open);
					form.Add(new StreamContent(stream), name, Path.GetFileName(path));
				}
			}
		}
	}
}
