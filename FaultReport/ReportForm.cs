﻿using System;
using System.Windows.Forms;

namespace FaultReport
{
	using Properties;

	public partial class ReportForm : Form
	{
		private Report report = null;

		public ReportForm(Report report)
		{
			this.report = report;
			InitializeComponent();
		}

		private void ReportForm_Load(object sender, EventArgs e)
		{
			Text = Locale.FormTitle;
			labelHeader.Text = Locale.LabelHeader;
			buttonClose.Text = Locale.ButtonClose;
			buttonCancel.Text = Locale.ButtonCancel;
			buttonSendReport.Text = Locale.ButtonReport;
			
			textBoxDetails.Text = (report?.Message == null)
				? Locale.DetailsGenericMessage
				: string.Format(Locale.DetailsFormat, report.Message);
			
			buttonCancel.Visible = Program.SendReport;
			buttonSendReport.Visible = Program.SendReport;
			buttonClose.Visible = !Program.SendReport;
			
			if(Program.SendReport)
			{
				buttonSendReport.Focus();
			}
			else
			{
				buttonClose.Focus();
			}
		}

		private void buttonClose_Click(object sender, EventArgs e)
		{
			Program.OnCancel();
			Close();
		}

		private void buttonCancel_Click(object sender, EventArgs e)
		{
			Program.OnCancel();
			Close();
		}

		private async void buttonSendReport_Click(object sender, EventArgs e)
		{
			Visible = false;
			await Program.OnUpload();
			Close();
		}
	}
}
