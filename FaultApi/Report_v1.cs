using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;

using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.WindowsAzure.Storage.Blob;

namespace FaultApi
{
	public class ReportEntity : TableEntity
	{
		// PartitionKey = AppId
		// RowKey = unique GUID
		public string Version { get; set; }
		public string Message { get; set; }
		public DateTime? Created { get; set; }
		public string DumpBlob { get; set; }
		public string LogBlob { get; set; }
	}

	public class ReportContent
	{
		public string AppId { get; private set; } = null;
		public string AppVersion { get; private set; } = null;
		public string Message { get; private set; } = null;
		public DateTime? Created { get; private set; } = null;
		public Stream DumpStream { get; private set; } = null;
		public string DumpName { get; private set; } = null;
		public Stream LogStream { get; private set; } = null;
		public string LogName { get; private set; } = null;

		public async Task ParseContent(MultipartMemoryStreamProvider multipart)
		{
			foreach (var content in multipart.Contents)
			{
				var name = content.Headers.ContentDisposition.Name;

				if (name == "appId")
				{
					AppId = await content.ReadAsStringAsync();
				}
				else if (name == "appVersion")
				{
					AppVersion = await content.ReadAsStringAsync();
				}
				else if (name == "message")
				{
					Message = await content.ReadAsStringAsync();
				}
				else if (name == "created")
				{
					string date = await content.ReadAsStringAsync();
					if (DateTime.TryParse(date, out DateTime dateTime))
					{
						Created = dateTime;
					}
				}
				else if (name == "dump")
				{
					DumpName = content.Headers.ContentDisposition.FileName;
					DumpStream = await content.ReadAsStreamAsync();
				}
				else if (name == "log")
				{
					LogName = content.Headers.ContentDisposition.FileName;
					LogStream = await content.ReadAsStreamAsync();
				}
			}
		}
	}

	public static class Report_v1
	{
		[FunctionName("Report_v1")]
		public static async Task<HttpResponseMessage> Run(
			[HttpTrigger(AuthorizationLevel.Function, "post", Route = "v1/report")] HttpRequestMessage req,
			[Table("Report")] CloudTable reportTable,
			[Blob("dumps")] CloudBlobContainer dumps,
			[Blob("logs")] CloudBlobContainer logs,
			TraceWriter log)
		{
			var form = await req.Content.ReadAsMultipartAsync();

			var content = new ReportContent();
			await content.ParseContent(form);

			if (string.IsNullOrEmpty(content.AppId))
			{
				return req.CreateResponse(HttpStatusCode.BadRequest);
			}

			var guid = Guid.NewGuid();
			string blobDump = null;
			string blobLog = null;

			if (content.DumpStream != null)
			{
				blobDump = $"{guid.ToString()}_{content.DumpName}";
				if (await UploadBlob(dumps, blobDump, content.DumpStream) == false)
				{
					log.Error($"Failed to upload blob {blobDump}!");
					return req.CreateResponse(HttpStatusCode.InternalServerError);
				}
			}

			if (content.LogStream != null)
			{
				blobLog = $"{guid.ToString()}_{content.LogName}";
				if (await UploadBlob(logs, blobLog, content.LogStream) == false)
				{
					log.Error($"Failed to upload blob {blobLog}!");
					return req.CreateResponse(HttpStatusCode.InternalServerError);
				}
			}

			var entity = new ReportEntity
			{
				PartitionKey = content.AppId,
				RowKey = guid.ToString(),
				Message = content.Message,
				Version = content.AppVersion,
				Created = content.Created,
				DumpBlob = blobDump,
				LogBlob = blobLog
			};

			var insert = TableOperation.Insert(entity);
			TableResult result = await reportTable.ExecuteAsync(insert);

			if (result.HttpStatusCode != 204)
			{
				log.Error($"Failed to create report! ({result.HttpStatusCode})");
				return req.CreateResponse(HttpStatusCode.InternalServerError);
			}

			return req.CreateResponse(HttpStatusCode.OK);
		}

		private static async Task<bool> UploadBlob(CloudBlobContainer target, string name, Stream file)
		{
			var blob = target.GetBlockBlobReference(name);

			if (await blob.ExistsAsync())
			{
				return false;
			}

			await blob.UploadFromStreamAsync(file);
			return true;
		}
	}
}
