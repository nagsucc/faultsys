#include <iostream>
#include <sstream>
#include <string>

// Include FaultLib header and library
#include <FaultLib.h>
#pragma comment(lib, "FaultLib.lib")

// This function will try to divide by zero.
// It's laid out this way to confuse the compiler.
// Otherwise, the compiler refuses to process this.
void faulty()
{
	faultLogInfo("Begin crashing!");
	int i = 0;
	char ayyy[10] = {};
	sprintf_s(ayyy, 10, "Boom %d", 5 / i); // Boom boom boom
}

// This is our callback function which the exception handler will call.
void callback(FAULT_CALLBACK_DATA* pData)
{
	// Let's make a cryptic error message no mortal can comprehend
	std::string message;
	{
		std::stringstream info;

		info << std::hex
			<< "An exception " << pData->pExceptionInfo->ExceptionRecord->ExceptionCode
			<< " was thrown at " << pData->pExceptionInfo->ExceptionRecord->ExceptionAddress
			<< ".";

		message = info.str();
	}

	faultLogTrace(message);

	// Set crash reporter dialog message
	faultSetMessage(message.c_str());
}

int main()
{
	// FaultLib Configuration
	FAULT_CONFIG conf(
		"test",							// Application identifier
		"test.exe",						// Applicaton name
		"1.0",							// Application version
		"",								// Working directory (use current)
		FaultModeFlag::kModeGui,		// Mode	(use GUI)
		FaultSettingsFlag::kDefault,	// Settings (default: kUseOfflineCache | kClearFiles)
		FaultReportFlag::kDefault,		// Report configuration (default: kIncludeDump | kIncludeLog | 	kIncludeStackTrace | kIncludeSystemInfo)
		nullptr,						// Custom handler (use default)
		callback,						// Callback	function
		MiniDumpNormal					// MiniDump type flag
	);

	// Initialize FaultLib
	faultInit(conf);

	// Configurate logger
	faultSetLogLevel(FAULT_LOG_LEVEL_TRACE);	// Set log level to Trace
	faultSetLogFilePath("test.log");			// Write log to file
	faultSetLogOutputStream(std::cout);			// Print log to console

	faultLogInfo("FaultLib Initialized");

	faulty(); // This will crash the program

	// We won't reach this point...
	faultUninit();
	return 0;
}
